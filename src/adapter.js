var youbora = require('youboralib')
var manifest = require('../manifest.json')

var SpotXAdapter = youbora.adapter.extend({
  /** Override to return current plugin version */
  getVersion: function () {
    return manifest.version + '-' + manifest.name + '-' + manifest.tech
  },

  /** Override to return current playhead of the video */
  getPlayhead: function () {
    return this.currentTime
  },

  /** Override to return title */
  getTitle: function () {
    if (!this.getCurrentAd()) return null

    return this.getCurrentAd().title
  },

  /** Override to return video duration */
  getDuration: function () {
    if (!this.getCurrentAd()) return null

    return this.getCurrentAd().duration
  },

  /** Override to return current bitrate */
  getBitrate: function () {
    if (!this.getCurrentAd()) return null

    return this.getCurrentAd().bitrate
  },

  /** Override to return rendition */
  getRendition: function () {
    if (!this.getCurrentAd()) return null

    var ad = this.getCurrentAd()

    return youbora.Util.buildRenditionString(
      ad.size.width,
      ad.size.height,
      ad.bitrate
    )
  },

  /** Override to return resource URL. */
  getResource: function () {
    if (!this.getCurrentAd()) return null

    return this.getCurrentAd().resource
  },

  /** Override to return player version */
  getPlayerVersion: function () {
    return this.playerVersion
  },

  /** Override to return player's name */
  getPlayerName: function () {
    return 'Spotx'
  },

  /** Override to return current ad position (only ads) */
  getPosition: function () {
    if (this.plugin.getAdapter() && !this.plugin.getAdapter().flags.isJoined) {
      return youbora.Constants.AdPosition.Preroll
    }

    return youbora.Constants.AdPosition.Midroll
  },

  /** Override to return the number of ads given for the break (only ads) */
  getGivenAds: function () {
    if (!this.ads) {
      return null
    }

    return this.ads.length
  },

  /** Override to return if the ad is being shown in the screen or not
   * The standard definition is: more than 50% of the pixels of the ad are on the screen
   * (only ads)
   */
  getIsVisible: function () {
    var container = this.player.slot()

    if (container && !this.isInternetExplorer()) {
      return youbora.Util.calculateAdViewability(this.player.slot())
    }

    return null
  },

  /** Override to return a boolean showing if the audio is enabled when the ad begins (only ads) */
  getAudioEnabled: function () {
    if (this.adInfo) {
      return this.adInfo.mute
    }
    return null
  },

  /** Override to return a boolean showing if the player is in fullscreen mode when the ad begins (only ads) */
  getIsFullscreen: function () {
    var container = this.player.slot()

    if (container && container.clientWidth && container.clientHeight) {
      return (
        window.innerHeight <= container.clientHeight + 60 &&
        window.innerWidth <= container.clientWidth + 60
      )
    }

    return null
  },

  getCurrentAdNumber: function () {
    return (this.plugin.requestBuilder.lastSent.adNumber || 1) - 1
  },

  /** Register listeners to this.player. */
  registerListeners: function () {
    // Enable playhead monitor (buffer = true, seek = false)
    this.monitorPlayhead(true, false)

    if (!this.isInternetExplorer()) {
      this.buildAdsArray()
    }

    this.references = {
      AdPaused: this.adPause.bind(this),
      AdLoaded: this.adLoaded.bind(this),
      AdStarted: this.adStarted.bind(this),
      AdImpression: this.adImpression.bind(this),
      AdStopped: this.adStopped.bind(this),
      AdSkipped: this.adSkipped.bind(this),
      AdVideoStart: this.adVideoStart.bind(this),
      AdVideoFirstQuartile: this.adVideoFirstQuartile.bind(this),
      AdVideoMidpoint: this.adVideoMidpoint.bind(this),
      AdVideoThirdQuartile: this.adVideoThirdQuartile.bind(this),
      AdVideoComplete: this.adVideoComplete.bind(this),
      AdClickThru: this.adClickThru.bind(this),
      AdUserClose: this.adUserClose.bind(this),
      AdPlaying: this.adPlaying.bind(this),
      AdError: this.adError.bind(this),
      AdRemainingTimeChange: this.adRemainingTimeChange.bind(this),
      AdVolumeChange: this.adVolumeChange.bind(this),
      AdLog: this.onAdLog.bind(this)
    }

    for (var key in this.references) {
      this.player.subscribe(this.references[key], key)
    }
  },

  /** Unregister listeners to this.player. */
  unregisterListeners: function () {
    if (this.player) {
      for (var key in this.references) {
        this.player.unsubscribe(this.references[key], key)
      }
    }
  },

  adPause: function (e) {
    this.firePause()
  },

  adStarted: function (e) {
    this.tryToUnregisterAdapterListeners()
    this.fireStart()
  },

  adLoaded: function (e) {},

  adImpression: function (e) { this.fireStart() },

  adStopped: function (e) {
    this.fireStop()
    this.fireBreakStop()
    this.tryToRegisterAdapterListeners()
  },

  adSkipped: function (e) {
    this.fireSkip()
  },

  adVideoStart: function (adInfo) {
    this.fireStart()
    this.fireJoin()
    this.updateAdInfo(adInfo)
  },

  adVideoFirstQuartile: function (e) {
    this.fireQuartile(1)
  },

  adVideoMidpoint: function (e) {
    this.fireQuartile(2)
  },

  adVideoThirdQuartile: function (e) {
    this.fireQuartile(3)
  },

  adVideoComplete: function (e) {
    this.fireStop()
    this.adInfo = undefined
  },

  adClickThru: function (e) {
    if (!this.ads) {
      this.fireClick()
    } else {
      this.fireClick({ adUrl: this.ads[this.getCurrentAdNumber()].pageUrl })
    }
  },

  adUserClose: function (e) {
    this.fireSkip()
  },

  adPlaying: function (e) {
    this.fireResume()
  },

  adError: function (e) {
    this.tryToUnregisterAdapterListeners()
    if (typeof e === 'string') {
      this.fireFatalError('unknown', e)
      this.tryToRegisterAdapterListeners()
      return
    }

    this.fireFatalError()
    this.tryToRegisterAdapterListeners()
  },

  adRemainingTimeChange: function (e) {
    this.currentTime = e.duration - this.player.getAdRemainingTime()
  },

  adVolumeChange: function (adInfo) {
    this.updateAdInfo(adInfo)
  },

  onAdLog: function (adInfo) {
    this.updateAdInfo(JSON.parse(adInfo))
  },

  // Get Ads array for all browsers
  getCreative: function (elements) {
    for (var index in elements) {
      var element = elements[index]

      if (element.localName === 'VAST') {
        this.playerVersion = element.attributes.version.nodeValue
      }

      if (element.localName === 'Creatives') {
        return element
      }
    }
    return undefined
  },

  getCreatives: function (creative) {
    if (!creative) return undefined

    var numberOfElements = creative.childElementCount

    var creativeList = []
    for (var i = 0; i < numberOfElements; i++) {
      var element = creative.children[i]
      var jsonResult = element.textContent.match(/\{.*:\{.*:.*\}/)[0]
      creativeList.push(JSON.parse(jsonResult))
    }

    return creativeList
  },

  // Get Ads array for all firebase in android

  // Get all children recursively until find "Creative" node
  getCreativeAndroidFirefox: function (children) {
    var element = children[0]

    if (element.nodeName !== 'InLine') {
      return this.getCreativeAndroidFirefox(element.children)
    }

    for (var i = 0; i < element.children.length; i++) {
      var childrenElement = element.children[i]
      if (childrenElement.nodeName === 'Creatives') {
        return childrenElement.children
      }
    }

    return undefined
  },

  getCreativesAndroidFirefox: function (creatives) {
    if (!creatives) return undefined

    var numberOfElements = creatives.length

    var creativeList = []
    for (var i = 0; i < numberOfElements; i++) {
      var jsonResult = creatives[i].textContent.match(/\{.*:\{.*:.*\}/)[0]
      creativeList.push(JSON.parse(jsonResult))
    }

    return creativeList
  },

  buildAdsArray: function () {
    this.player.getVASTAd().then(
      function (result) {
        var jsonCreatives

        var isAndroid =
          navigator.userAgent.toLowerCase().indexOf('android') > -1
        var isFirefox =
          navigator.userAgent.toLowerCase().indexOf('firefox') > -1

        if (isAndroid && isFirefox) {
          jsonCreatives = this.getCreativesAndroidFirefox(
            this.getCreativeAndroidFirefox(result.children)
          )
        } else {
          jsonCreatives = this.getCreatives(this.getCreative(result.all))
        }

        this.ads = jsonCreatives.map(function (jsonData) {
          var videoKey = Object.keys(jsonData.media.video)[0]

          return {
            id: jsonData.ad_id,
            title: jsonData.title,
            source: jsonData.media.ad_source,
            bitrate: jsonData.media.video[videoKey].bitrate,
            resource: jsonData.media.video[videoKey].media_url,
            duration: jsonData.media.video[videoKey].playtime,
            pageUrl: jsonData.media.video[videoKey].page_url,
            size: {
              width: jsonData.media.video[videoKey].width,
              height: jsonData.media.video[videoKey].height
            }
          }
        })
      }.bind(this)
    )
  },

  tryToRegisterAdapterListeners: function () {
    if (this.plugin && this.plugin.getAdapter()) {
      this.plugin.getAdapter().registerListeners()
    }
  },

  tryToUnregisterAdapterListeners: function () {
    if (this.plugin && this.plugin.getAdapter()) {
      this.plugin.getAdapter().unregisterListeners()
    }
  },

  getCurrentAd: function () {
    if (this.isInternetExplorer()) return this.adInfo

    if (!this.ads) return null

    return this.ads[this.getCurrentAdNumber()]
  },

  isInternetExplorer: function () {
    return window.navigator.userAgent.indexOf('MSIE ') > 0 || !!navigator.userAgent.match(/Trident.*rv:11\./)
  },

  updateAdInfo: function (object) {
    if (!this.adInfo) {
      this.adInfo = object
      return
    }

    this.adInfo = Object.assign(this.adInfo, object)
  }
})

module.exports = SpotXAdapter
